---
title: Childcare
---

PyGotham is pleased to partner with Sitters Studio to provide free childcare for
2018.

## About Sitters Studio

Sitters Studio is excited to provide childcare for the PyGotham event. Sitters Studio provides
high-quality childcare services to New York and Chicago families, but their unique twist is that all
of their caregivers are artists who are pursuing a career in their art discipline. Sitter Studio is
a registered and insured company and all of their Artisitters go through a thorough vetting process
including: reference checks, an FCRA background check, and several in-person interviews. They are
also First Aid and CPR Certified.


## Sitters Studio Childcare Policy

Because the health and safety of those taking advantage of our childcare room
is important to us, standard daycare policies will apply. Children must have
current vaccinations and are free from illness upon arrival. Any child who is
ill at drop off will not be permitted in the childcare room. If a child starts
exhibiting any signs of illness, parents and/or guardians will be contacted
for immediate pick up. As to be mindful of those with food allergies, the
childcare area is a nut free zone.

We look forward to providing a fun and safe environment for all families!

## Registration

Childcare is available to registered PyGotham attendees. Space is limited, and
signups will be accepted on a first come, first served basis. Hurry, registration
is only open until September 27th.
[Register here](https://docs.google.com/forms/d/18_JWwK7khuWzr3_VYbJXF8BfcxOodh1BLQ7V0Zzln7k/viewform).
