---
title: "PyGotham 2018 has started!!"
date: 2018-10-05 08:00:00 -0400
excerpt_separator: <!--more-->
---

PyGotham 2018 has started!!

<!--more-->

Here are a few useful links to guide you through the conference.

* [Lightning Talk Signup](https://docs.google.com/spreadsheets/d/1Zx2FocjVFmQtdiPyJZEG_hJRQqT1wmJrC8eIhqlmfAc/edit?usp=sharing)
* [Open Spaces Signup](https://docs.google.com/spreadsheets/d/1VDpWGKvPoCjI-yFVPKM2waDIOoYDXQljX7V-5bql2A0/edit?usp=sharing)
* [Schedule](/talks/schedule/)
* [Social Event](https://2018.pygotham.org/events/socialevent/)
* [Young Coders](https://2018.pygotham.org/events/youngcoders/)
