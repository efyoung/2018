---
abstract: At Instagram, we've successfully utilized python's asyncio library to reduce
  the latency of our server side endpoints; however, understanding latency in an asyncio
  deployment isn't so easy.  In this talk, we'll discuss the techniques we've used
  to measure and improve latency.
duration: 30
level: All
presentation_url:
room: Madison
slot: 2018-10-06 10:45:00-04:00
speakers:
- Mark Vismonte
title: 'Not so fast: Understanding Latency in Asyncio-powered Systems'
type: talk
---

At Instagram, measuring and improving endpoint latency is a primary concern.  Reducing endpoint latency helps improve server capacity as well as improves a user's experience on the Instagram app.  By leveraging asyncio, we're able to drive down latency; however, latency in a cooperatively multitasked system is very complex to understand and optimize. This is because the runtime of individual tasks are affected by tasks that run around it.  We'll discuss different examples of how latency is largely influenced by context and the tools we utilize to further understand where our coroutines are spending their time.
