---
duration: 45
presentation_url:
room: PennTop South
slot: 2018-10-06 16:15:00-04:00
speakers:
- Gloria W.
title: Where to begin? How to End? The mental meanderings of a lifelong geek
type: talk
video_url: https://youtu.be/EnoY0f_GuG4
---

I'll talk about the engineering phenomenon known as "bags of water",
transporting brains, the power of distant utility lights, people who throw
up in taxis, chaos and order, small talk with strangers, and how I
inadvertently got addicted to software development. Fun times!
