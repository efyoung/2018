---
name: James Powell
talks:
- Write a Git Client from Scratch
---

James Powell is a professional Python programmer and enthusiast. He started working with Python in the finance industry building reporting and analysis systems for prop trading front offices. He currently works as a consultant buliding data engineering and scientific computing platforms for a wide-range of clients using cutting-edge open source
tools like Python and React. He is the leading technical consultant for the Telecom Infa Project, which is bringing industry-standard software engineering practices to the fields of optical and backbone engineering.

In his spare time, he serves as a Vice President at NumFOCUS, the 501(c)3 non-profit that supports all the major tools in the Python data analysis ecosystem (incl. `pandas`, `numpy`, `jupyter`, `matplotlib`, and others.) At NumFOCUS, he helps build global open source communities for data scientists, data engineers, and business analysis. He helps NumFOCUS run the PyData conference series, and has sat on speaker selection and organizing committees for thirty-one conferences. James is also a prolific speaker: since 2013, he has given over seventy conference talks at over fifty Python events worldwide.