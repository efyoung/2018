---
name: Paul Ganssle
talks:
- 'Gathering Related Functionality: Patterns for Clean API Design'
---

Paul Ganssle is a software developer at Bloomberg and maintains the library python-dateutil. He lives in New York City and is interested in programming, languages, wearable electronics and sensors.

Expressions of opinion do not necessarily reflect the views of his employer.